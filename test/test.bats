#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/datadog-send-event"}

  echo "Building image..."
  run docker build -t ${DOCKER_IMAGE} .
}

teardown() {
  rm -rf $(pwd)/tmp/pipe-*.txt
}

@test "Sending event to Datadog should succeed" {
    run docker run \
        -e API_KEY=$API_KEY \
        -e TITLE="Title" \
        -e TEXT="Description" \
        -e PRIORITY="low" \
        -e ALERT_TYPE="error" \
        -e TAGS="\"mytag1:foo\", \"mytag2:bar\"" \
        -v $(pwd)/tmp:/tmp \
        -w $(pwd) \
    ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]

    request=$(cat $(pwd)/tmp/pipe-*.txt)

    [ "$(echo $request | jq -r '.status')" = "ok" ]
    [ "$(echo $request | jq -r '.event.title')" = "Title" ]
    [ "$(echo $request | jq -r '.event.text')" = "Description" ]
    [ "$(echo $request | jq -r '.event.priority')" = "low" ]
    [ "$(echo $request | jq -r '.event.tags[0]')" = "mytag1:foo" ]
    [ "$(echo $request | jq -r '.event.tags[1]')" = "mytag2:bar" ]
}

@test "Event is sent successfully with double quote in text" {

    TEXT="Pipelines is awesome: \" $RANDOM!"

    run docker run \
        -e API_KEY=$API_KEY \
        -e TITLE="Title" \
        -e TEXT="$TEXT" \
        -e PRIORITY="low" \
        -e ALERT_TYPE="error" \
        -e TAGS="\"mytag1:foo\", \"mytag2:bar\"" \
        -v $(pwd)/tmp:/tmp \
        -w $(pwd) \
    ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]

    request=$(cat $(pwd)/tmp/pipe-*.txt)

    [ "$(echo $request | jq -r '.status')" = "ok" ]
}

